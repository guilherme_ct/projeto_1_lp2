-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 30/09/2019 às 01:42
-- Versão do servidor: 10.1.13-MariaDB
-- Versão do PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `projeto_1_lp2_guilherme_costa`
--

CREATE DATABASE `projeto_1_lp2_guilherme_costa`;

USE `projeto_1_lp2_guilherme_costa`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `conteudo_contato`
--

CREATE TABLE `conteudo_contato` (
  `id` int(11) NOT NULL,
  `tituloIntro` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `conteudo_contato`
--

INSERT INTO `conteudo_contato` (`id`, `tituloIntro`, `descricao`, `endereco`, `telefone`, `email`, `last_modified`) VALUES
(1, 'Entre em contato conosco!', 'Entre em contato conosco através do formulário abaixo que iremos te responder em até 3 horas!', 'Guarulhos, SP, Brasil', '(11) 1929-9215', 'contato@wallet.com', '2019-09-21 19:13:16');

-- --------------------------------------------------------

--
-- Estrutura para tabela `conteudo_empresa`
--

CREATE TABLE `conteudo_empresa` (
  `id` int(11) NOT NULL,
  `tituloIntro` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `subtitulo1` varchar(255) NOT NULL,
  `texto1` varchar(255) NOT NULL,
  `subtitulo2` varchar(255) NOT NULL,
  `texto2` varchar(255) NOT NULL,
  `subtitulo3` varchar(255) NOT NULL,
  `texto3` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `conteudo_empresa`
--

INSERT INTO `conteudo_empresa` (`id`, `tituloIntro`, `titulo`, `descricao`, `imagem`, `subtitulo1`, `texto1`, `subtitulo2`, `texto2`, `subtitulo3`, `texto3`, `last_modified`) VALUES
(1, 'Conheça nossa empresa!', 'Wallet Soluções Financeiras', 'Conheça um pouco da nossa empresa e de nossos princípios.', 'wallet_logo.png', 'Quem somos nós?', 'Somos uma empresa de tecnologia que desenvolve plataformas digitais com o objetivo de melhorar a vida financeira das pessoas.', 'História', 'A Wallet surgiu em 2016 com o propósito de melhorar a qualidade de vida das pessoas através da administração financeira. Sempre com transparência, confiança, segurança e a melhor qualidade de serviço possível!', 'Equipe', 'Nossa equipe é formada por desenvolvedores, designers e especialistas no mercado financeiro. Somos uma equipe jovem e determinada, pronta para atender à qualquer necessidade dos nossos clientes.', '2019-09-21 18:59:38');

-- --------------------------------------------------------

--
-- Estrutura para tabela `conteudo_home`
--

CREATE TABLE `conteudo_home` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` longtext NOT NULL,
  `texto` longtext NOT NULL,
  `imagem1` mediumtext NOT NULL,
  `imagem2` mediumtext NOT NULL,
  `imagem3` mediumtext NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `conteudo_home`
--

INSERT INTO `conteudo_home` (`id`, `titulo`, `descricao`, `texto`, `imagem1`, `imagem2`, `imagem3`, `last_modified`) VALUES
(1, 'Aprenda a utilizar seu dinheiro da forma correta !', 'Com o aplicativo da Wallet você pode aprender como utilizar seu dinheiro da forma correta e controlar os seus gastos. Experimente agora na faixa!\r\n', 'O aplicativo Wallet te ensina a melhor forma de economizar e controlar seus gastos, através de uma plataforma intuitiva, dinâmica, colorida e fácil de utilizar. O Wallet gera gráficos baseados em seus gastos e ganhos, permitindo que você administre da melhor forma o seu dinheiro!\r\n\r\n', 'fundo_home.jpg', 'celulares_home.png', '', '2019-09-16 23:23:11');

-- --------------------------------------------------------

--
-- Estrutura para tabela `conteudo_produto`
--

CREATE TABLE `conteudo_produto` (
  `id` int(11) NOT NULL,
  `tituloIntro` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `texto1` varchar(255) NOT NULL,
  `texto2` varchar(255) NOT NULL,
  `texto3` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `conteudo_produto`
--

INSERT INTO `conteudo_produto` (`id`, `tituloIntro`, `titulo`, `descricao`, `texto1`, `texto2`, `texto3`, `last_modified`) VALUES
(1, 'Veja as vantagens de utilizar o nosso produto!', 'Porque o Wallet é sua melhor opção?', 'Veja as vantagens de utilizar o Wallet para administrar seus ganhos e gastos', 'O Wallet gera gráficos baseados nas seus despesas e ganhos, tornando claro e simples como está a sua evolução financeira.', 'A plataforma Wallet oferece diversos materiais e vídeos que melhoram sua vida financeira. Que vão desde como você pode cortar gastos desnecessários, até investimentos.', 'Nossa empresa oferece suporte à você sempre que precisar, entre em contato conosco através do nosso telefone ou do nosso e-mail que entramos em contato com você em no máximo 3 horas!', '2019-09-21 18:15:58');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `conteudo_contato`
--
ALTER TABLE `conteudo_contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `conteudo_empresa`
--
ALTER TABLE `conteudo_empresa`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `conteudo_home`
--
ALTER TABLE `conteudo_home`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `conteudo_produto`
--
ALTER TABLE `conteudo_produto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `conteudo_contato`
--
ALTER TABLE `conteudo_contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `conteudo_empresa`
--
ALTER TABLE `conteudo_empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `conteudo_home`
--
ALTER TABLE `conteudo_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `conteudo_produto`
--
ALTER TABLE `conteudo_produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
