<?php 
class Contatolib extends CI_Object {

    public function get(){
        $rs = $this->db->get('conteudo_contato');
        return $rs->row_array();
    }

    public function update($data, $id){
        $cond['id'] = $id;
        return $this->db->update('conteudo_contato',$data, $cond);
    }
}