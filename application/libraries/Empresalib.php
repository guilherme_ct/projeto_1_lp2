<?php 
class Empresalib extends CI_Object {

    public function get(){
        $rs = $this->db->get('conteudo_empresa');
        return $rs->row_array();
    }

    public function update($data, $id){
        $cond['id'] = $id;
        return $this->db->update('conteudo_empresa',$data, $cond);
    }
}