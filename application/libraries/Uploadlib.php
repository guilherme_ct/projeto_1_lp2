<?php 
class Uploadlib extends CI_Object {

    public function enviar_arquivo($name_arquivo){
        
        $config['upload_path'] = './assets/img';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);
        
        $this->upload->do_upload($name_arquivo);
    }

}
