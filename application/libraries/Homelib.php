<?php 
class Homelib extends CI_Object {

    public function get(){
        $rs = $this->db->get('conteudo_home');
        return $rs->row_array();
    }

    public function update($data, $id){
        $cond['id'] = $id;
        return $this->db->update('conteudo_home',$data, $cond);
    }
}