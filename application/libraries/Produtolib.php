<?php 
class Produtolib extends CI_Object {

    public function get(){
        $rs = $this->db->get('conteudo_produto');
        return $rs->row_array();
    }

    public function update($data, $id){
        $cond['id'] = $id;
        return $this->db->update('conteudo_produto',$data, $cond);
    }
}