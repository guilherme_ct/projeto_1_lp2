<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class EmpresaModel extends CI_Model {

    public function get_conteudo(){
        $this->load->library('empresalib');
        return $this->empresalib->get();
    }

    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data['tituloIntro'] = $this->input->post('tituloIntro');
        $data['titulo'] = $this->input->post('titulo');
        $data['descricao'] = $this->input->post('descricao');
        $data['subtitulo1'] = $this->input->post('subtitulo1');
        $data['texto1'] = $this->input->post('texto1');
        $data['subtitulo2'] = $this->input->post('subtitulo2');
        $data['texto2'] = $this->input->post('texto2');
        $data['subtitulo3'] = $this->input->post('subtitulo3');
        $data['texto3'] = $this->input->post('texto3');

        $this->load->library('empresalib');
        $status = $this->empresalib->update($data, $id);
        $this->session->set_flashdata('sucesso', 'Atualizado com sucesso!');
        if($status) redirect('painel/empresa/textos');
    }

    public function atualizar_arquivo($id){
        if(sizeof($_FILES) == 0) return;

        $imagem=$_FILES['imagem']['name'];
        $tamanho_imagem=$_FILES['imagem']['size'];
        if($tamanho_imagem>0){
            $data['imagem'] = $imagem; 
        }
     
        $this->load->library('empresalib');
        $this->empresalib->update($data, $id);
        
    }
    
}