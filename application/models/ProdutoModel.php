<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoModel extends CI_Model {

    public function get_conteudo(){
        $this->load->library('produtolib');
        return $this->produtolib->get();
    }

    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data['tituloIntro'] = $this->input->post('tituloIntro');
        $data['titulo'] = $this->input->post('titulo');
        $data['descricao'] = $this->input->post('descricao');
        $data['texto1'] = $this->input->post('texto1');
        $data['texto2'] = $this->input->post('texto2');
        $data['texto3'] = $this->input->post('texto3');

        $this->load->library('produtolib');
        $status = $this->produtolib->update($data, $id);
        $this->session->set_flashdata('sucesso', 'Atualizado com sucesso!');
        if($status) redirect('painel/produto');
    }
    
}