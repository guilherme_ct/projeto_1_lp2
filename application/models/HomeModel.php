<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model {

    public function get_conteudo(){
        $this->load->library('homelib');
        return $this->homelib->get();
    }

    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data['titulo'] = $this->input->post('titulo');
        $data['descricao'] = $this->input->post('descricao');
        $data['texto'] = $this->input->post('texto');
     
        $this->load->library('homelib');
        $status = $this->homelib->update($data, $id);
        $this->session->set_flashdata('sucesso', 'Atualizado com sucesso!');
        if($status) redirect('painel');
    }

    public function atualizar_arquivo($id){
        if(sizeof($_FILES) == 0) return;

        $imagem1=$_FILES['imagem1']['name'];
        $tamanho_imagem1=$_FILES['imagem1']['size'];
        $imagem2=$_FILES['imagem2']['name'];
        $tamanho_imagem2=$_FILES['imagem2']['size'];
        
        if($tamanho_imagem1>0){
            $data['imagem1'] = $imagem1; 
        }
        if($tamanho_imagem2>0){
            $data['imagem2'] = $imagem2; 
        }
     
        $this->load->library('homelib');
        $this->homelib->update($data, $id);
        
    }
    
}