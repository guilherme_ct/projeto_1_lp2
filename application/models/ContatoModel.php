<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ContatoModel extends CI_Model {

    public function get_conteudo(){
        $this->load->library('contatolib');
        return $this->contatolib->get();
    }

    public function atualizar($id){
        if(sizeof($_POST) == 0) return;

        $data['tituloIntro'] = $this->input->post('tituloIntro');
        $data['descricao'] = $this->input->post('descricao');
        $data['endereco'] = $this->input->post('endereco');
        $data['telefone'] = $this->input->post('telefone');
        $data['email'] = $this->input->post('email');

        $this->load->library('contatolib');
        $status = $this->contatolib->update($data, $id);
        $this->session->set_flashdata('sucesso', 'Atualizado com sucesso!');
        if($status) redirect('painel/contato');
    }
    
}