<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['painel/home/textos'] = 'painel/home_textos';
$route['painel/home/imagens'] = 'painel/home_imagens';
$route['painel/empresa/textos'] = 'painel/empresa_textos';
$route['painel/empresa/imagens'] = 'painel/empresa_imagens';
