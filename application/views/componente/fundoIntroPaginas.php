 <!-- Full Page Intro -->
 <div class="fundoIntro" style="background-color:#0d47a1; background-repeat: no-repeat; background-size: cover;  background-position: center center;">
    <!-- Mask & flexbox options-->
    <div class="align-items-center">
        <!-- Content -->
        <div class="container">
            <!--Grid row-->
            <div class="row">

                <div class="col-md-12 mx-auto col-xl-6 mt-xl-5">
                    <!--Grid row-->
                    <div class="row">
                        <!--Grid column-->
                        <div class="col-md-12 white-text text-center  mt-xl-5 mb-5">
                            <h1 class="h1-responsive font-weight-bold mt-sm-5"> <?php echo $tituloIntro ?> </h1>
                            <hr class="hr-light">
                            <i class="fa fa-angle-double-down mt-3"></i>
                        </div>
                        <!--Grid column-->
                        
                    </div>
                    <!--Grid row-->
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
        </div>
        <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
</div>
<!-- Full Page Intro -->
</header>
<!-- Main navigation -->
