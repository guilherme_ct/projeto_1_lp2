<!-- Footer -->
<footer class="page-footer font-small blue pt-4 mt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Wallet - Soluções Financeiras</h5>
        <p>Aprenda a utilizar seu dinheiro da forma correta!</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Links</h5>

        <ul class="list-unstyled">
          <li>
            <a href="<?php echo base_url('') ?>">Início</a>
          </li>
          <li>
            <a href="<?php echo base_url('produto') ?>">Produto</a>
          </li>
          <li>
            <a href="<?php echo base_url('empresa') ?>">Empresa</a>
          </li>
          <li>
            <a href="<?php echo base_url('contato') ?>">Contato</a>
          </li>
          <li>
            <a href="<?php echo base_url('painel') ?>" target="_blank">Painel Administrativo</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->



      <!--
    
      <div class="col-md-3 mb-md-0 mb-3">

        <h5 class="text-uppercase">Links</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!">Link 1</a>
          </li>
          <li>
            <a href="#!">Link 2</a>
          </li>
          <li>
            <a href="#!">Link 3</a>
          </li>
          <li>
            <a href="#!">Link 4</a>
          </li>
        </ul>

      </div>
      -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Wallet - Soluções Financeiras
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->