 <div class="view" style="background-image: url('<?php echo base_url('assets/img/'.$imagem1) ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="mask rgba-gradient align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 white-text text-center text-md-left mt-xl-5 mb-5 wow fadeInLeft" data-wow-delay="0.3s" style="margin-top:-45px">
                    <h1 class="h1-responsive font-weight-bold mt-sm-5"> <?php echo isset($titulo) ? $titulo : '' ?> </h1>
                    <hr class="hr-light">
                    <h6 class="mb-4"><?php echo isset($descricao) ? $descricao : '' ?>
                    </h6>
                    <a class="btn btn-outline-white" href="<?php echo base_url('produto') ?>">Veja mais</a>
                </div>
                <div class="col-md-6 col-xl-5 mt-xl-5 wow fadeInRight" data-wow-delay="0.3s">
                    <img src="<?php echo base_url('assets/img/'.$imagem2) ?>" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
</header>

<main>
    <div class="container">
        <div class="row py-5">
            <div class="col-md-12 text-center">
            <p><?php echo isset($texto) ? $texto : '' ?></p>
            </div>
        </div>
    </div>
</main>
