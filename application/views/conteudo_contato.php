<?php
if(isset($_POST['sucesso'])){ 
  $this->session->set_flashdata('sucesso', 'Agradecemos o contato!'); 
}
?>
<!-- Section: Contact v.2 -->
<section class="my-5">

  <!-- Section description -->
  <p class="text-center w-responsive mx-auto mb-5">
  <?php echo isset($descricao) ? $descricao : '' ?>  
  </p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-9 mb-md-0 mb-5">

      <form method="post" action="">
      <?php
      if($this->session->flashdata('sucesso')){
      ?>
      <div class="alert alert-success alert-dismissible" role="alert">
          <?= $this->session->flashdata('sucesso'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <?php
      }
      ?>

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-6">
            <div class="md-form mb-0">
              <input type="text" id="contact-name" class="form-control">
              <label for="contact-name" class="">Seu nome</label>
            </div>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6">
            <div class="md-form mb-0">
              <input type="text" id="contact-email" class="form-control">
              <label for="contact-email" class="">Seu email</label>
            </div>
          </div>
          <!-- Grid column -->

        </div>
 
        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-12">
            <div class="md-form mb-0">
              <input type="text" id="contact-Subject" class="form-control">
              <label for="contact-Subject" class="">Assunto</label>
            </div>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-12">
            <div class="md-form">
              <textarea id="contact-message" class="form-control md-textarea" rows="3"></textarea>
              <label for="contact-message">Mensagem</label>
            </div>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      <input type="hidden" name="sucesso">

      <div class="text-center text-md-left">
        <button class="btn btn-primary btn-md" type="submit">Enviar</button>
      </div>

      </form>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-3 text-center">
      <ul class="list-unstyled mb-0">
        <li>
          <i class="fas fa-map-marker-alt fa-2x blue-text"></i>
          <p><?php echo isset($endereco) ? $endereco : '' ?></p>
        </li>
        <li>
          <i class="fas fa-phone fa-2x mt-4 blue-text"></i>
          <p><?php echo isset($telefone) ? $telefone : '' ?></p>
        </li>
        <li>
          <i class="fas fa-envelope fa-2x mt-4 blue-text"></i>
          <p class="mb-0"><?php echo isset($email) ? $email : '' ?></p>
        </li>
      </ul>
    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</section>
<!-- Section: Contact v.2 -->