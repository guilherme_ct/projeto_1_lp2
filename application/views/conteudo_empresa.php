<!-- Section: Features v.3 -->
<section class="my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5"><?php echo isset($titulo) ? $titulo : '' ?></h2>
  <!-- Section description -->
  <p class="lead grey-text text-center w-responsive mx-auto mb-5"><?php echo isset($descricao) ? $descricao : '' ?></p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-4 text-center">
      <img class="img-fluid" src="<?php echo base_url('assets/img/'.$imagem) ?>" alt="Sample image">
    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3">

        <!-- Grid column -->
        <div class="col-1">
          <i class="fas fa-question fa-lg indigo-text"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
          <h5 class="font-weight-bold mb-3"><?php echo isset($subtitulo1) ? $subtitulo1 : '' ?></h5>
          <p class="grey-text"> <?php echo isset($texto1) ? $texto1 : '' ?> </p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">

        <!-- Grid column -->
        <div class="col-1">
          <i class="fas fa-book fa-lg indigo-text"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
          <h5 class="font-weight-bold mb-3"><?php echo isset($subtitulo2) ? $subtitulo2 : '' ?> </h5>
          <p class="grey-text">
          <?php echo isset($texto2) ? $texto2 : '' ?>
          </p>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!--Grid row-->
      <div class="row">

        <!-- Grid column -->
        <div class="col-1">
          <i class="fas fa-users fa-lg indigo-text"></i>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-xl-10 col-md-11 col-10">
          <h5 class="font-weight-bold mb-3"><?php echo isset($subtitulo3) ? $subtitulo3 : '' ?></h5>
          <p class="grey-text mb-0">
          <?php echo isset($texto3) ? $texto3 : '' ?>          
          </p>
        </div>
        <!-- Grid column -->

      </div>
      <!--Grid row-->

    </div>
    <!--Grid column-->

  </div>
  <!-- Grid row -->

</section>
<!-- Section: Features v.3 -->