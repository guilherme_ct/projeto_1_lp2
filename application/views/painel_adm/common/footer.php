</div>
    <!-- JavaScript files-->
    <script src="<?php echo base_url('assets/painel_adm/vendor/jquery/jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/painel_adm/vendor/popper.js/umd/popper.min.js')?>"> </script>
    <script src="<?php echo base_url('assets/painel_adm/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/painel_adm/js/grasp_mobile_progress_circle-1.0.0.min.js')?>"></script>
    <script src="<?php echo base_url('assets/painel_adm/vendor/jquery.cookie/jquery.cookie.js')?>"> </script>
    <script src="<?php echo base_url('assets/painel_adm/vendor/chart.js/Chart.min.js')?>"></script>
    <script src="<?php echo base_url('assets/painel_adm/vendor/jquery-validation/jquery.validate.min.js')?>"></script>
    <script src="<?php echo base_url('assets/painel_adm/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')?>"></script>
    <!-- Main File-->
    <script src="<?php echo base_url('assets/painel_adm/js/front.js')?>"></script>
  </body>
</html>