
    <!-- Side Navbar -->
    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <!-- User Info-->
          <div class="sidenav-header-inner text-center">
            <h2 class="h5">Wallet</h2><span>Painel Administrativo</span>
          </div>
          <!-- Small Brand information, appears on minimized sidebar-->
          <div class="sidenav-header-logo"><a  class="brand-small text-center"> <strong>W</strong> </a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
          <h5 class="sidenav-heading">Menu</h5>
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a href="#home" aria-expanded="false" data-toggle="collapse"> <i class="icon-home"></i>Home </a>
              <ul id="home" class="collapse list-unstyled ">
                <li><a href="<?php echo base_url('painel/home/textos') ?>">Editar Textos</a></li>
                <li><a href="<?php echo base_url('painel/home/imagens') ?>">Editar Imagens</a></li>
              </ul>
            </li>            
            <li><a href="<?php echo base_url('painel/produto') ?>"> <i class="fa fa-dollar"></i>Produto </a></li>
            <li><a href="#empresa" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-building"></i>Empresa </a>
              <ul id="empresa" class="collapse list-unstyled ">
                <li><a href="<?php echo base_url('painel/empresa/textos') ?>">Editar Textos</a></li>
                <li><a href="<?php echo base_url('painel/empresa/imagens') ?>">Editar Imagens</a></li>
              </ul>
            </li>   
            <li><a href="<?php echo base_url('painel/contato') ?>"> <i class="fa fa-phone"></i>Contato</a></li>
            
          </ul>
        </div>
       
      </div>
    </nav>
    <div class="page">