
  <section class="forms">
    <div class="container-fluid">
      <!-- Page Header-->
      <header> 
        <h1 class="h3 display">Home</h1>
      </header>
      <div class="row">

        <div class="col-lg-12">
          <div class="card">
            <div class="card-header d-flex align-items-center">
              <h4>Editar Conteúdo</h4>
            </div>
            <div class="card-body">
              <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                <?php
                if($this->session->flashdata('sucesso')){
                ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    Atualizado com sucesso!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php
                }
                ?>
                

                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Título</label>
                  <div class="col-sm-10">
                    <input type="text" name="titulo" class="form-control" value="<?php echo isset($titulo) ? $titulo : '' ?>">
                  </div>
                </div>
                <div class="line"></div>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Descrição</label>
                  <div class="col-sm-10">
                    <textarea name="descricao" id="descricao" rows="5" class="form-control"><?php echo isset($descricao) ? $descricao : '' ?></textarea>
                  </div>
                </div>
                <div class="line"></div>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Texto</label>
                  <div class="col-sm-10">
                  <textarea name="texto" id="texto" rows="5" class="form-control"><?php echo isset($texto) ? $texto : '' ?></textarea>
                  </div>
                </div>

                  <input type="hidden" name="sucesso" value="ss">
                <div class="line"></div>
                <div class="form-group row">
                  <div class="col-sm-4 offset-sm-2">
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    