
      <section class="forms">
        <div class="container-fluid">
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">Empresa</h1>
          </header>
          <div class="row">

            <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Editar Conteúdo</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal" action="" method="POST">
                    <?php
                    if($this->session->flashdata('sucesso')){
                    ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        Atualizado com sucesso!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                    }
                    ?>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Título Intro</label>
                      <div class="col-sm-10">
                        <input type="text" name="tituloIntro" class="form-control" value="<?php echo isset($tituloIntro) ? $tituloIntro : '' ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Título Conteúdo</label>
                      <div class="col-sm-10">
                        <input type="text" name="titulo" value="<?php echo isset($titulo) ? $titulo : '' ?>" class="form-control">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Descrição</label>
                      <div class="col-sm-10">
                        <input type="text" name="descricao" class="form-control" value="<?php echo isset($descricao) ? $descricao : '' ?>">
                      </div>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Subtítulo 1</label>
                      <div class="col-sm-10">
                        <input type="text" name="subtitulo1" class="form-control" value="<?php echo isset($subtitulo1) ? $subtitulo1 : '' ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row ml-3">
                      <label class="col-sm-2 form-control-label">Texto 1</label>
                      <div class="col-sm-10">
                        <textarea name="texto1" id="texto1" rows="5" class="form-control"><?php echo isset($texto1) ? $texto1 : '' ?></textarea>
                      </div>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Subtítulo 2</label>
                      <div class="col-sm-10">
                        <input type="text" name="subtitulo2" class="form-control" value="<?php echo isset($subtitulo2) ? $subtitulo2 : '' ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row ml-3">
                      <label class="col-sm-2 form-control-label">Texto 2</label>
                      <div class="col-sm-10">
                        <textarea name="texto2" id="texto2" rows="5" class="form-control"><?php echo isset($texto2) ? $texto2 : '' ?></textarea>
                      </div>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Subtítulo 3</label>
                      <div class="col-sm-10">
                        <input type="text" name="subtitulo3" class="form-control" value="<?php echo isset($subtitulo3) ? $subtitulo3 : '' ?>">
                      </div>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row ml-3">
                      <label class="col-sm-2 form-control-label">Texto 3</label>
                      <div class="col-sm-10">
                        <textarea name="texto3" id="texto3" rows="5" class="form-control"><?php echo isset($texto3) ? $texto3 : '' ?></textarea>
                      </div>
                    </div>


                    <div class="line"></div>
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <button type="submit" class="btn btn-primary">Atualizar</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    