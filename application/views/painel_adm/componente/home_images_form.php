
  <section class="forms">
    <div class="container-fluid">
      <!-- Page Header-->
      <header> 
        <h1 class="h3 display">Home</h1>
      </header>
      <div class="row">

        <div class="col-lg-12">
          <div class="card">
            <div class="card-header d-flex align-items-center">
              <h4>Editar Imagens</h4>
            </div>
            <div class="card-body">
            
                <?php echo form_open_multipart('');?>
                                
                <div class="line"></div>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Imagem 1</label>
                  <div class="col-sm-10">
                    <img src="<?php echo base_url('assets/img/'.$imagem1) ?>" width="200">
                    <input type="file" name="imagem1" class="form-control">       
                  </div>
                </div>

                <div class="line"></div>
                <div class="form-group row">
                  <label class="col-sm-2 form-control-label">Imagem 2</label>
                  <div class="col-sm-10">
                    <img src="<?php echo base_url('assets/img/'.$imagem2) ?>" width="200">
                    <input type="file" name="imagem2" class="form-control">     
                  </div>
                </div>

                <div class="line"></div>
                <div class="form-group row">
                  <div class="col-sm-4 offset-sm-2">
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    