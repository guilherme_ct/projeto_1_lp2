
      <section class="forms">
        <div class="container-fluid">
          <!-- Page Header-->
          <header> 
            <h1 class="h3 display">Contato</h1>
          </header>
          <div class="row">

            <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Editar Conteúdo</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal" action="" method="POST">
                    <?php
                    if($this->session->flashdata('sucesso')){
                    ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        Atualizado com sucesso!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php
                    }
                    ?>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Título Intro</label>
                      <div class="col-sm-10">
                        <input type="text" name="tituloIntro" class="form-control" value="<?php echo isset($tituloIntro) ? $tituloIntro : '' ?>">
                      </div>
                    </div>
                   
                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Descrição</label>
                      <div class="col-sm-10">
                        <input type="text" name="descricao" class="form-control" value="<?php echo isset($descricao) ? $descricao : '' ?>">
                      </div>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Endereço</label>
                      <div class="col-sm-10">
                        <input type="text" name="endereco" class="form-control" value="<?php echo isset($endereco) ? $endereco : '' ?>">
                      </div>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Telefone</label>
                      <div class="col-sm-10">
                        <input type="text" name="telefone" class="form-control" value="<?php echo isset($telefone) ? $telefone : '' ?>">
                      </div>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" value="<?php echo isset($email) ? $email : '' ?>">
                      </div>
                    </div>


                    <div class="line"></div>
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <button type="submit" class="btn btn-primary">Atualizar</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    