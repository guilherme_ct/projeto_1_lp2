<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
  <div class="container">

    <!-- Brand -->
    <a class="navbar-brand waves-effect" href="<?php echo base_url() ?>">
      <strong class="white-text">WALLET</strong>
    </a>

    <!-- Collapse -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Links -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <!-- Left -->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link waves-effect" href="<?php echo base_url('home') ?>">Início
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect" href="<?php echo base_url('produto') ?>">Produto
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect" href="<?php echo base_url('empresa') ?>">Empresa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect" href="<?php echo base_url('contato') ?>">Contato</a>
        </li>
      </ul>

      <!-- Right -->
      <ul class="navbar-nav nav-flex-icons">
        <li class="nav-item">
         <!-- <a class="nav-link waves-effect" data-toggle="modal" onclick="" data-target="#modalForm">-->
         <a class="nav-link waves-effect" href="<?php echo base_url('painel') ?>" target="_blank">  
            <i class="fa fa-cog"></i>
          </a>
        </li>
      </ul>

    </div>

  </div>
</nav>
