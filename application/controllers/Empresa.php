<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends MY_Controller {

        public function index()
        {
                $this->load->model('EmpresaModel', 'empresa');
                $this->empresa->atualizar(1);
                $conteudo = $this->empresa->get_conteudo();
                $fundoIntro = $this->load->view('componente/fundoIntroPaginas', $conteudo, true);
                $html=$this->load->view('conteudo_empresa.php',$conteudo,true);
                $this->carregar($html, $fundoIntro);

        }
}
