<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$this->load->model('HomeModel', 'home');
		$this->home->atualizar(1);

		$data = $this->home->get_conteudo();
		$html = $this->load->view('painel_adm/componente/home_form', $data, true);
		$this->carregar_painel($html);	
	}
	

	public function home_textos()
	{
		$this->load->model('HomeModel', 'home');
		$this->home->atualizar(1);

		$data = $this->home->get_conteudo();
        $html = $this->load->view('painel_adm/componente/home_form', $data, true);
		$this->carregar_painel($html);	
	}

	public function home_imagens()
	{
		$this->load->model('HomeModel', 'home');
		$this->load->library('uploadlib');
		
		$this->uploadlib->enviar_arquivo('imagem1');
		$this->uploadlib->enviar_arquivo('imagem2');

		$this->home->atualizar_arquivo(1);

		$data = $this->home->get_conteudo();
		$html = $this->load->view('painel_adm/componente/home_images_form', $data, true);
		
		$this->carregar_painel($html);	
	}

	public function produto()
	{
		$this->load->model('ProdutoModel', 'produto');
		$this->produto->atualizar(1);

		$data = $this->produto->get_conteudo();
        $html = $this->load->view('painel_adm/componente/produto_form', $data, true);
		$this->carregar_painel($html);	
	}

	public function empresa_textos()
	{
		$this->load->model('EmpresaModel', 'empresa');
		$this->empresa->atualizar(1);

		$data = $this->empresa->get_conteudo();
        $html = $this->load->view('painel_adm/componente/empresa_form', $data, true);
		$this->carregar_painel($html);	
	}

	public function empresa_imagens()
	{
		$this->load->model('EmpresaModel', 'empresa');
		$this->load->library('uploadlib');
		
		$this->uploadlib->enviar_arquivo('imagem');

		$this->empresa->atualizar_arquivo(1);

		$data = $this->empresa->get_conteudo();
		$html = $this->load->view('painel_adm/componente/empresa_images_form', $data, true);
		
		$this->carregar_painel($html);	
	}

	public function contato()
	{
		$this->load->model('ContatoModel', 'contato');
		$this->contato->atualizar(1);

		$data = $this->contato->get_conteudo();
        $html = $this->load->view('painel_adm/componente/contato_form', $data, true);
		$this->carregar_painel($html);	
	}
}
