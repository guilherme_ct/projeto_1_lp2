<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends MY_Controller {

	public function index()
	{
                $this->load->model('ContatoModel', 'contato');                
                $conteudo = $this->contato->get_conteudo();
                
                //Introducao
                $fundoIntro = $this->load->view('componente/fundoIntroPaginas', $conteudo, true);
                
                //Conteúdo
                $html=$this->load->view('conteudo_contato',$conteudo,true);

                $this->carregar($html, $fundoIntro);
	}
}
