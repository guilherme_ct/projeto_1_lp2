<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends MY_Controller {

	public function index()
	{
        $this->load->model('ProdutoModel', 'produto');
        $data = $this->produto->get_conteudo();
        $fundoIntro = $this->load->view('componente/fundoIntroPaginas.php', $data, true);
        $html=$this->load->view('conteudo_produto', $data, true);
		$this->carregar($html, $fundoIntro);
	}
}
