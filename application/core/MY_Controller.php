<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{

    public function carregar($conteudo, $fundoHome){
        // incluir o cabeçalho
        $html = '<header>';
        $html .= $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbar', null, true);
        $html .= $fundoHome;
        $html .= '</header>';

        // incluir o conteúdo
        $html .= '<div class="container">';
        $html .= $conteudo;
        $html .= '</div>';

        // incluir o rodapé
        $html .= $this->load->view('componente/rodape', null, true);
        $html .= $this->load->view('common/footer', null, true);
        echo $html;
    }

    public function carregar_painel($conteudo){
        // incluir o cabeçalho
        $html = $this->load->view('painel_adm/common/header', null, true);
        $html .= $this->load->view('painel_adm/common/sidebar', null, true);
        $html .= $this->load->view('painel_adm/common/navbar', null, true);

         // incluir o conteúdo
        $html .= $conteudo;

        // incluir o rodapé
        $html .= $this->load->view('painel_adm/common/rodape', null, true);
        $html .= $this->load->view('painel_adm/common/footer', null, true);
        echo $html;
    }

}